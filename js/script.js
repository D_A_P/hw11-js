'use strict';

const btn = document.getElementById("btn-click");

btn.addEventListener('click', () =>{
    const btn = document.getElementById("btn-click");
    const newElement = document.createElement("p");
    newElement.textContent = "New Paragraph";
    btn.after(newElement);      
})


const newBtn = document.createElement("button");
newBtn.id = 'btn-input-create';
newBtn.textContent = "Button";
const wrapperBtn = document.createElement("div");
wrapperBtn.append(newBtn);
const content = document.getElementById("content");
content.append(wrapperBtn);

newBtn.addEventListener('click', ()=>{
    const newInput = document.createElement("input");
    newInput.type = "text;"
    newInput.placeholder = "Your name";
    newInput.name = "userName";    
    const wrapperInput = document.createElement("div");
    wrapperInput.append(newInput);
    newBtn.after(wrapperInput);
    
});



